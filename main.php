<?php
require 'core.php';
require 'db_connect.php';

include 'utils.php';

if (loggedin()) {
	echo 'You\'re logged in. <a href="logout.php">Log out</a>';
} else {
	include 'loginform.php';
	include 'register.php';
}


?>


<!DOCTYPE html>
<html>
<head>

<!-- your webpage info goes here -->

    <title>Facultatea de Automatică și Calculatoare - Portal Studenți</title>

	<meta name="author" content="your name" />
	<meta name="description" content="" />

<!-- you should always add your stylesheet (css) in the head tag so that it starts loading before the page html is being displayed -->
	<link rel="stylesheet" href="style.css" type="text/css" />

</head>
<body>

<!-- webpage content goes here in the body -->

	<div id="page">
		<div id="logo">
			<h1><a href="/" id="logoLink">Portal Studenți</a></h1>
		</div>
		<div id="nav">
			<ul>
				<li><a href="#/home.html">Home</a></li>
				<li><a href="#/situatie.html">Situație scolară</a></li>
				<li><a href="#/contact.html">Contact</a></li>
                <li><a href="#/intrebari.html">Întrebări frecvente</a></li>
				<li><a href="#/observatii.html">Întrebări şi observaţii </a></li>
			</ul>
		</div>
		<div id="content">
			<h2>Home</h2>
			<p>
                Bine ați venit pe portalul studenților ACS!
            </p>
            <p>
                Informații generale
			</p>
			<p>
            Începând cu data de 20 Septembrie, fiecare student îşi va putea accesa contul, creat pe baza celui de la admitere (http://admitere.pub.ro).
            </p>
            <p>
            Autentificarea se face cu numele de utilizator, iar parola aceeaşi cu cea de la admitere.
			</p>
            <p>
            Pentru clarificări şi observaţii în ceea ce priveşte modul de funcţionare a site-ului, vă rugăm să consultaţi secţiunile:
                       <p> <li>Întrebări frecvente</li> </p>
                       <p> <li>Întrebări şi observaţii</li> </p>
            </p>
		</div>
		<div id="footer">
			<p>
				Webpage made by <a href="/" target="_blank">[ByteMe Team]</a>
			</p>
		</div>
	</div>
</body>
</html>