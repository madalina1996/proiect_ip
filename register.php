<?php
require 'core.php';

include 'db_connect.php';

function loggedin1() {
    if (isset($_SESSION['user_id'])&& !empty($_SESSION['user_id'])) {
        return true;
    }
    return false;
}

$username='';
$surname='';
$firstname='';
$type='';

if (!loggedin1()) {

    if (isset($_POST['username']) &&
isset($_POST['password']) &&
isset($_POST['password_again']) &&
isset($_POST['surname']) &&
isset($_POST['firstname']) &&
isset($_POST['type'])) {
    $username = $_POST['username'];
    $password = $_POST['password'];
    $password_again = $_POST['password_again'];
    $surname = $_POST['surname'];
    $firstname = $_POST['firstname'];
    $type = $_POST['type'];

    if(!empty($username) &&
    !empty($password) &&
    !empty($password_again) &&
    !empty($surname) &&
    !empty($firstname) &&
    !empty($type)) {
        if ($password != $password_again) {
            echo 'Passwords do not match.';
        } else {
            $query = "SELECT `username` FROM `users` WHERE `username`='$username'";
            $query_run = mysqli_query($con, $query);
            if(mysqli_num_rows($query_run) == 1) {
                echo 'The username '.$username.' already exists.';
            } else {
                $query2 = "INSERT INTO `users`(`username`, `password`, `first_name`, `last_name`, `type`) VALUES
                ('$username','$password','$firstname','$surname','$type')";
                if ($query_run2 = mysqli_query($con, $query2)) {
                    echo 'You\'re registered.';
                    header('Location: register_success.php');
                } else {
                    echo 'Sorry, we couldn\'t register you at this time. Try again later.';
                }
            }
        }
    } else {
        echo 'All fields are required.';
    }
}

?>

<html>
    <head>
    <link rel="stylesheet" href="style.css" type="text/css" />

    <form action="register.php" method="POST">
    Username:<br> <input type="text" name="username" value="<?php echo $username; ?>"><br><br>
    Parola:<br> <input type="password" name="password"><br><br>
    Confirmare parola:<br> <input type="password" name="password_again"><br><br>
    Nume:<br> <input type="text" name="surname" value="<?php echo $surname; ?>"><br><br>
    Prenume:<br> <input type="text" name="firstname" value="<?php echo $firstname; ?>"><br><br>
    Tip utilizator:<br> <input type="text" name="type" value="<?php echo $type; ?>"><br><br>
    <input type="submit" value="Register">

    </form>

    </head>
</html>

<?php
} else {
    echo 'You\'re already registered and logged in.';
}

?>